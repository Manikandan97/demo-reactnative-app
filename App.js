/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import type {Node} from 'react';
import TouchID from 'react-native-touch-id';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Alert,
  useColorScheme,
  TouchableHighlight,
  View,
} from 'react-native';
import {Platform, NativeModules} from 'react-native';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const App: () => Node = () => {
  useEffect(() => {
    // generateKeysFromMnemonics();
    // generateHederaAccount()
  }, []);

  const generateKeysFromMnemonics = async mnemonics => {
    try {
      return new Promise(async (resolve, reject) => {
        console.log('resolve', resolve);
        if (Platform.OS === 'ios') {
          // let account = await _generateHederaAccount()
          // console.log(account);
          // resolve(account);
          var HashgraphJavaSDK = NativeModules.HashgraphApi;
          HashgraphJavaSDK.from_mnemonic_for_mobile(
            mnemonics,
            (error, data) => {
              console.log(error);
              if (!error) {
                let parsedData = JSON.parse(data);
                if (parsedData.status === 'success') {
                  let data = parsedData.message;
                  data.transactionHistory = [];
                  data.passPhase = mnemonics;
                  data.mnemonic = mnemonics;
                  data.accountName = 'Default Account';
                  data.secretKey = data.key.split(' ')[1];
                  data.privateKey = data.key.split(' ')[1];
                  data.publicKey = data.key.split(' ')[0];
                  resolve(JSON.stringify(data));
                } else {
                  resolve(parsedData);
                }
                // let account = {
                //     mnemonic: data.mnemonic,
                //     publicKey: data.publicKey,
                //     privateKey: data.privateKey
                // }
                // resolve(account)
              } else {
                reject(error);
              }
            },
          );
        } else {
          var HashgraphJavaSDK = NativeModules.HashgraphJavaSDK;

          console.log('HashgraphJavaSDK', HashgraphJavaSDK);
          HashgraphJavaSDK.importAccount(mnemonics, (error, data) => {
            console.log('data', data);
            if (!error) {
              let account = {
                mnemonic: data.mnemonic,
                publicKey: data.publicKey,
                privateKey: data.privateKey,
              };
              resolve(account);
            } else {
              reject(error);
            }
          });
        }
      });
    } catch (error) {
      reject(error);
    }
  };

  const generateHederaAccount = async callback => {
    try {
      return new Promise(async (resolve, reject) => {
        console.log('resolve');
        var HashgraphJavaSDK = null;
        if (Platform.OS === 'ios') {
          HashgraphJavaSDK = NativeModules.HashgraphApi;

          HashgraphJavaSDK.createAccount((error, data) => {
            if (!error) {
              let parsedData = JSON.parse(data);
              let account = {
                mnemonic: parsedData.message.passPhase,
                publicKey: parsedData.message.publicKey,
                privateKey: parsedData.message.secretKey,
              };
              // console.log(account);
              resolve(account);
            } else {
              reject(error);
            }
          });
        } else {
          HashgraphJavaSDK = NativeModules.HashgraphJavaSDK;

          console.log('HashgraphJavaSDK', HashgraphJavaSDK);
          HashgraphJavaSDK.createAccount((error, data) => {
            if (!error) {
              let account = {
                mnemonic: data.mnemonic,
                publicKey: data.publicKey,
                privateKey: data.privateKey,
              };
              resolve(account);
            } else {
              reject(error);
            }
          });
        }
      });
    } catch (error) {
      reject(error);
      // callback(error, null);
    }
  };

  const optionalConfigObject = {
    title: 'LOGIN', // Android
    imageColor: '#e00606', // Android
    imageErrorColor: '#ff0000', // Android
    sensorDescription: 'YOUR FINNGER PRINT PLEASE', // Android
    sensorErrorDescription: 'Failed', // Android
    cancelText: 'Cancel', // Android
    fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
    unifiedErrors: false, // use unified error messages (default false)
    passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
  };

  // const optionalConfigObject = {
  //   unifiedErrors: false, // use unified error messages (default false)
  //   passcodeFallback: false, // if true is passed, itwill allow isSupported to return an error if the device is not enrolled in touch id/face id etc. Otherwise, it will just tell you what method is supported, even if the user is not enrolled.  (default false)
  // };

  const _pressHandler = async => {
    TouchID.isSupported(optionalConfigObject)
      .then(biometryType => {
        // Success code
        if (biometryType === 'FaceID') {
          console.log('FaceID is supported.');
        } else {
          console.log('TouchID is supported.');
          TouchID.authenticate(
            'cHECK yOUR lOGIN',
            optionalConfigObject,
          )
            .then(success => {
              console.log('success', success);
              alert('Authenticated Successfully');
            })
            .catch(error => {
              alert('Authentication Failed');
            });
        }
      })
      .catch(error => {
        // Failure code
        console.log(error);
      });
  };

  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView
      style={{alignItems: 'center', justifyContent: 'center', height: '100%'}}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <Text>Hi text</Text>
      <TouchableHighlight
        onPress={() => {
          _pressHandler();
        }}>
        <Text>To Authenticate with Touch ID</Text>
      </TouchableHighlight>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
